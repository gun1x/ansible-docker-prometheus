# Ansible - Docker - Prometheus

This repo uses ansible to deploy prometheus to docker containers. Feel free to fork, but if you make modifications I would be curious to see what you did so please make a pull request.

## Features

* deploys prometheus
* sets up service discovery to get servers from file
* deploys alertmanager
* configures alertmanager to send alerts to slack
* deploys grafana
* creates systemd timers to update server list

__You will have to edit `config/update_server_list.sh` with your script, in order to get a proper server list__

The host has to be archlinux for pacman to work, but you can change the playbook to use apt/dnf/emerge instead.
